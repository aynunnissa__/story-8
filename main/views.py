from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
import json

def home(request):
	return render(request, 'main/home.html')

def search(request):
	search_text = request.GET.get('search_text', '')
	tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + search_text
	google_books = requests.get(tujuan)
	data = json.loads(google_books.content)
	return JsonResponse(data, safe=False)