from django.test import LiveServerTestCase, TestCase, tag, Client
from .views import search

class BookListTestCase(TestCase):
    def test_url_home(self):
        request = Client().get('/')
        self.assertEquals(request.status_code, 200)

    def test_template_home(self):
        request = Client().get('/')
        self.assertTemplateUsed(request, 'main/home.html')

    def test_search_view(self):
        data = {'search_text': 'Frozen'}
        request = Client().get('/search/', '')
        request.GET = data
        cari = search(request)
        cari.client = Client()
        self.assertEquals(cari.status_code, 200)
