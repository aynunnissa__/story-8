$(function() {
    $('#keyword').keyup(function() {
    	$.ajax({
            url: "/search/",
            data: {
                'search_text' : $('#keyword').val(),
            },
            success: function(hasil){
            	var obj_hasil = $('tbody');
            	obj_hasil.empty();

            	for (i = 0; i < hasil.items.length; i++) {
            		var title = hasil.items[i].volumeInfo.title;
            		var image = "'{% static 'images/main/default.jpg' %}'";
            		if (hasil.items[i].volumeInfo.imageLinks != null) {
            			var image = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
            		}

                    var authors = "-";

                    if (hasil.items[i].volumeInfo.authors != null) {
                        var authors = hasil.items[i].volumeInfo.authors[0];

                        for (j = 1; hasil.items[i].volumeInfo.authors.length > 0 && j < hasil.items[i].volumeInfo.authors.length; j++) {
                            authors += ", " + hasil.items[i].volumeInfo.authors[i];
                        }    
                    }
                	
            		obj_hasil.append(
            			"<tr><td class='thPict'><img src=" + image + "></td><td class='thJudul'><p>" + title + "</p></td><td class='thAuthor'><p>" + authors + "</p></td></tr>"
            		);

            	}	
            }
        });
    });
});